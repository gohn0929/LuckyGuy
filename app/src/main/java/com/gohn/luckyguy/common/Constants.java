package com.gohn.luckyguy.common;

/**
 * Created by Vayne on 2016. 9. 22..
 */

public class Constants {
    public static final long ONE_MINUTE = 60 * 1000;
    public static final long ONE_HOUR = 60 * ONE_MINUTE;
    public static final long ONE_DAY = ONE_HOUR * 24;
    public static final long ONE_YEAR = ONE_DAY * 365;
}
