package com.gohn.luckyguy.common;

import android.app.Activity;
import android.widget.Toast;

import com.gohn.luckyguy.R;

/**
 * Created by Gohn on 15. 12. 10..
 */
public class BackPressCloseHandler {

    private long backKeyPressedTime = 0;
    private Toast toast;

    private Activity activity;

    public BackPressCloseHandler(Activity context) {
        this.activity = context;
    }

    public void onBackPressed() {
        if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            showGuide();
            return;
        }
        if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
            activity.finish();
            toast.cancel();
        }
    }

    public void showGuide() {
        toast = Toast.makeText(activity.getApplicationContext(), R.string.main_finish, Toast.LENGTH_SHORT);
        toast.show();
    }
}