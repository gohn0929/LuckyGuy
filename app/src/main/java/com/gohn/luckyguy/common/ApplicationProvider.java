package com.gohn.luckyguy.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Handler;

/**
 * Created by Vayne on 16. 7. 15..
 */
public class ApplicationProvider extends Application {

    private static ApplicationProvider Instance;
    public static volatile Handler applicationHandler = null;
    private static volatile Activity currentActivity = null;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Instance = this;
        context = getApplicationContext();
        applicationHandler = new Handler(getInstance().getMainLooper());
    }

    public static ApplicationProvider getInstance() {
        return Instance;
    }

    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        ApplicationProvider.currentActivity = currentActivity;
    }

    public static Context getContext() {
        return context;
    }
}
