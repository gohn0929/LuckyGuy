package com.gohn.luckyguy.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import java.util.HashMap;

/**
 * Created by Vayne on 16. 8. 1..
 */
public class PrefManager {
    private final static String DEFAULT_PREFERENCE_NAME = "luckyguy";


    private static HashMap<String, PrefManager> prefMap = new HashMap<>();
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    static private Context context;

    private PrefManager(Context context, String preferenceName) {
        this.pref = context.getSharedPreferences(preferenceName, Activity.MODE_PRIVATE);
        this.editor = pref.edit();
    }

    public static PrefManager getInstance() {
        context = ApplicationProvider.getContext();

        if (!prefMap.containsKey(DEFAULT_PREFERENCE_NAME)) {
            prefMap.put(DEFAULT_PREFERENCE_NAME, new PrefManager(context, DEFAULT_PREFERENCE_NAME));
        }
        return prefMap.get(DEFAULT_PREFERENCE_NAME);
    }

    public static PrefManager getInstance(String preferenceName) {
        context = ApplicationProvider.getContext();

        if (!prefMap.containsKey(preferenceName)) {
            prefMap.put(preferenceName, new PrefManager(context, preferenceName));
        }
        return prefMap.get(preferenceName);
    }

    public void setBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public boolean getBoolean(String key, boolean defValue) {
        boolean retVal = pref.getBoolean(key, defValue);
        return retVal;
    }

    public void setFloat(Context context, String key, float value) {
        editor.putFloat(key, value);
        editor.commit();
    }

    public float getFloat(String key) {
        return getFloat(key, 0.0f);
    }

    public float getFloat(String key, float defValue) {
        float retVal = pref.getFloat(key, defValue);
        return retVal;
    }

    public void setInt(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public int getInt(String key) {
        return getInt(key, 0);
    }

    public int getInt(String key, int defValue) {
        int retVal = pref.getInt(key, defValue);
        return retVal;
    }

    public void setLong(String key, long value) {
        editor.putLong(key, value);
        editor.commit();
    }

    public long getLong(String key) {
        return getLong(key, 0);
    }

    public long getLong(String key, int defValue) {
        long retVal = pref.getLong(key, defValue);
        return retVal;
    }

    public boolean isEncryptionKey(String key) {
        return false;
    }

    public void setString(String key, String value) {
        if (isEncryptionKey(key)) {
            value = Base64.encodeToString(value.getBytes(), 0);
        }
        editor.putString(key, value);
        editor.commit();
    }


    public String getString(String key) {
        return getString(key, "");
    }

    public String getString(String key, String defValue) {

        String retVal = pref.getString(key, defValue);
        if (isEncryptionKey(key)) {
            retVal = new String(Base64.decode(retVal, 0));
        }
        return retVal;
    }

    public void clearAll() {
        editor.clear();
        editor.commit();
    }

    public static void deleteAll() {
        for (String key : prefMap.keySet()) {
            prefMap.get(key).editor.clear();
            prefMap.get(key).editor.commit();
        }
    }
}

