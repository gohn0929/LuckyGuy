package com.gohn.luckyguy.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.gohn.luckyguy.R;
import com.gohn.luckyguy.util.Utils;
import com.gohn.luckyguy.view.materialbutton.MButton;

/**
 * Created by Vayne on 16. 8. 3..
 */
@SuppressWarnings("unchecked")
public class LDialog {
    public static abstract class OnDialogButtonClickListner {
        public abstract void onClickOK(String result);

        public void onClickCancel() {

        }
    }

    public interface OneButtonClickListener {
        void onClick();
    }

    public interface TwoButtonClickListener {
        void onClickOK();

        void onClickCancel();
    }

    public interface ThreeButtonClickListener {
        void onClickOK();

        void onClickCancel();

        void onClickOption();
    }

    public static void showOneButtonDialog(final Context context, String title, String content, final OneButtonClickListener listener) {
        showOneButtonDialog(context, title, content, null, listener);
    }

    public static void showOneButtonDialog(final Context context,
                                           String title,
                                           String content,
                                           String okText,
                                           final OneButtonClickListener listener) {
        LayoutInflater li = LayoutInflater.from(context);

        final View view = li.inflate(R.layout.dialog_button_one, null);

        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        alertDialogBuilder.setView(view);
        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_dialog_ok:
                        if (listener != null) listener.onClick();
                        alertDialog.dismiss();
                        break;
                }
            }
        };

        TextView txtTitle = (TextView) view.findViewById(R.id.txt_dialog_title);
        txtTitle.setVisibility(title == null ? View.GONE : View.VISIBLE);
        txtTitle.setText(title == null ? "" : title);

        TextView txtContent = (TextView) view.findViewById(R.id.txt_dialog_content);
        txtContent.setVisibility(content == null ? View.GONE : View.VISIBLE);
        txtContent.setText(content == null ? "" : content);

        MButton btnOK = (MButton) view.findViewById(R.id.btn_dialog_ok);
        btnOK.setText(okText == null ? context.getString(R.string.ok) : okText);

        btnOK.setOnClickListener(onClickListener);
    }

    public static void showTwoButtonDialog(final Context context, String title, String content, final TwoButtonClickListener listener) {
        showTwoButtonDialog(context, title, content, null, null, 0, listener);
    }

    public static void showTwoButtonDialog(final Context context, String title, String content, int width, final TwoButtonClickListener listener) {
        showTwoButtonDialog(context, title, content, null, null, width, listener);
    }

    public static void showTwoButtonDialog(final Context activity,
                                           String title,
                                           String content,
                                           String okText,
                                           String cancelText,
                                           int width,
                                           final TwoButtonClickListener listener) {
        LayoutInflater li = LayoutInflater.from(activity);

        final View view = li.inflate(R.layout.dialog_button_two, null);

        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(activity);
        alertDialogBuilder.setView(view);
        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        if (width > 0) {
            WindowManager.LayoutParams params = alertDialog.getWindow().getAttributes();
            params.width = Utils.dp2px(activity, width);
            alertDialog.getWindow().setAttributes(params);
        }

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_dialog_ok:
                        if (listener != null) listener.onClickOK();
                        alertDialog.dismiss();
                        break;
                    case R.id.btn_dialog_cancel:
                        if (listener != null) listener.onClickCancel();
                        alertDialog.dismiss();
                        break;
                }
            }
        };

        TextView txtTitle = (TextView) view.findViewById(R.id.txt_dialog_title);
        txtTitle.setVisibility(title == null ? View.GONE : View.VISIBLE);
        txtTitle.setText(title == null ? "" : title);

        TextView txtContent = (TextView) view.findViewById(R.id.txt_dialog_content);
        txtContent.setVisibility(content == null ? View.GONE : View.VISIBLE);
        txtContent.setText(content == null ? "" : content);

        MButton btnOK = (MButton) view.findViewById(R.id.btn_dialog_ok);
        btnOK.setText(okText == null ? activity.getString(R.string.ok) : okText);
        btnOK.setOnClickListener(onClickListener);

        MButton btnCancel = (MButton) view.findViewById(R.id.btn_dialog_cancel);
        btnCancel.setText(cancelText == null ? activity.getString(R.string.cancel) : cancelText);
        btnCancel.setOnClickListener(onClickListener);
    }

    public static void showThreeButtonDialog(final Context context, String title, String content, final ThreeButtonClickListener listener) {
        showThreeButtonDialog(context, title, content, null, null, null, listener);
    }

    public static void showThreeButtonDialog(final Context context,
                                             String title,
                                             String content,
                                             String okText,
                                             String cancelText,
                                             String optionText,
                                             final ThreeButtonClickListener listener) {
        LayoutInflater li = LayoutInflater.from(context);

        final View view = li.inflate(R.layout.dialog_button_three, null);

        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        alertDialogBuilder.setView(view);
        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_dialog_ok:
                        if (listener != null) listener.onClickOK();
                        alertDialog.dismiss();
                        break;
                    case R.id.btn_dialog_cancel:
                        if (listener != null) listener.onClickCancel();
                        alertDialog.dismiss();
                        break;
                    case R.id.btn_dialog_option:
                        if (listener != null) listener.onClickOption();
                        alertDialog.dismiss();
                        break;
                }
            }
        };

        TextView txtTitle = (TextView) view.findViewById(R.id.txt_dialog_title);
        txtTitle.setVisibility(title == null ? View.GONE : View.VISIBLE);
        txtTitle.setText(title == null ? "" : title);

        TextView txtContent = (TextView) view.findViewById(R.id.txt_dialog_content);
        txtContent.setVisibility(content == null ? View.GONE : View.VISIBLE);
        txtContent.setText(content == null ? "" : content);

        MButton btnOK = (MButton) view.findViewById(R.id.btn_dialog_ok);
        btnOK.setText(okText == null ? context.getString(R.string.ok) : okText);
        btnOK.setOnClickListener(onClickListener);

        MButton btnCancel = (MButton) view.findViewById(R.id.btn_dialog_cancel);
        btnCancel.setText(cancelText == null ? context.getString(R.string.cancel) : cancelText);
        btnCancel.setOnClickListener(onClickListener);

        MButton btnOption = (MButton) view.findViewById(R.id.btn_dialog_option);
        btnOption.setText(optionText == null ? context.getString(R.string.setting) : optionText);
        btnOption.setOnClickListener(onClickListener);
    }
}