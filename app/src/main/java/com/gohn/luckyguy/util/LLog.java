package com.gohn.luckyguy.util;

import android.util.Log;

import com.gohn.luckyguy.common.CommonData;

/**
 * Created by Gohn on 16. 6. 19..
 */
public class LLog {
    final static String TAG = "### LAB_GYMDAY ###";

    public static void e(String tag, String msg) {
        if (!CommonData.RELEASE)
            Log.e(tag, String.format("### ERROR ### %s", msg));
    }

    public static void w(String tag, String msg) {
        if (!CommonData.RELEASE)
            Log.w(tag, String.format("### WARNING ### %s", msg));
    }

    public static void d(String tag, String msg) {
        if (!CommonData.RELEASE)
            Log.d(tag, String.format("##### %s", msg));
    }

    public static void i(String tag, String msg) {
        if (!CommonData.RELEASE)
            Log.i(tag, msg);
    }

    public static void v(String tag, String msg) {
        if (!CommonData.RELEASE)
            Log.v(tag, msg);
    }

    public static void e(String msg) {
        if (!CommonData.RELEASE)
            Log.e(TAG, String.format("### ERROR ### %s", msg));
    }

    public static void w(String msg) {
        if (!CommonData.RELEASE)
            Log.w(TAG, String.format("### WARNING ### %s", msg));
    }

    public static void d(String msg) {
        if (!CommonData.RELEASE)
            Log.d(TAG, String.format("##### %s", msg));
    }

    public static void i(String msg) {
        if (!CommonData.RELEASE)
            Log.i(TAG, msg);
    }

    public static void v(String msg) {
        if (!CommonData.RELEASE)
            Log.v(TAG, msg);
    }
}
