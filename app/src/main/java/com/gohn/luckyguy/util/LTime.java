package com.gohn.luckyguy.util;

import com.gohn.luckyguy.common.Constants;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


/**
 * Created by Vayne on 16. 8. 12..
 */
public class LTime {
    private final static String[] DAYOFWEEK = {"일", "월", "화", "수", "목", "금", "토"};
    long milliseconds;

    public LTime(String time) {
        if (time.trim().isEmpty()) {
            milliseconds = new Date().getTime();
        } else {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));

            try {
                Date date = format.parse(time);
                this.milliseconds = date.getTime();
            } catch (ParseException e) {
                LLog.e(e.toString());
            }
        }
    }

    public LTime(long milliseconds) {
        this.milliseconds = milliseconds;
    }

    public String getFullDate() {
        Date date = new Date(milliseconds);
        DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        return formatter.format(date);
    }

    public String getServerFormatDate() {
        Date date = new Date(milliseconds);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return formatter.format(date);
    }

    public String getDate() {
        Date date = new Date(milliseconds);
        DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
        return formatter.format(date);
    }

    public String getSimpleDate() {
        Date date = new Date(milliseconds);
        DateFormat formatter = new SimpleDateFormat("M월 d일");

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        return String.format("%s (%s)", formatter.format(date), DAYOFWEEK[dayOfWeek - 1]);
    }

    public String getDateWithDash() {
        Date date = new Date(milliseconds);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    public String getDateWithPoint() {
        Date date = new Date(milliseconds);
        DateFormat formatter = new SimpleDateFormat("yy.MM.dd");
        return formatter.format(date);
    }

    public String getSimpleDateWithPoint() {
        Date date = new Date(milliseconds);
        DateFormat formatter = new SimpleDateFormat("MM.dd");
        return formatter.format(date);
    }

    public String getTime() {
        Date date = new Date(milliseconds);
        DateFormat formatter = new SimpleDateFormat("h:mm");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (cal.get(Calendar.AM_PM) == Calendar.AM) {
            return "오전 " + formatter.format(date);
        } else {
            return "오후 " + formatter.format(date);
        }
    }

    public String getHourAndMinute() {
        Date date = new Date(milliseconds);
        DateFormat formatter = new SimpleDateFormat("hh:mm");
        return formatter.format(date);
    }

    public long getTimestamp() {
        return milliseconds;
    }

    public long getTimestampInMinutes() {
        return milliseconds / 1000 / 60;
    }

    public LTime addDays(int days) {
        milliseconds += days * Constants.ONE_DAY;
        return this;
    }
}
