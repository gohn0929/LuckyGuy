package com.gohn.luckyguy.util;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.gohn.luckyguy.R;

/**
 * Created by Gohn on 2016. 10. 8..
 */

public class Utils {
    public static int dp2px(Context context, int dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    public static void toast(Activity activity, String msg) {
        Toast.makeText(activity.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public static void toast(Context context, String msg) {
        Toast.makeText(context.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public static boolean isNullOrEmpty(String text) {
        if (text == null) return true;
        if (text.trim().equals("")) return true;
        return false;
    }

    public static void hideKeyboard(Context context, View textview) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(textview.getWindowToken(), 0);
    }

    public static void showKeyboard(Context context, View textview) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(textview, 0);
    }

    public static void downloadFromUrl(Context context, String imageURL) {
        String servicestring = Context.DOWNLOAD_SERVICE;
        DownloadManager downloadmanager;
        downloadmanager = (DownloadManager) context.getSystemService(servicestring);
        Uri uri = Uri.parse(imageURL);
        DownloadManager.Request request = new DownloadManager.Request(uri);

        String fileName = imageURL.substring(imageURL.lastIndexOf("/"));

        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        downloadmanager.enqueue(request);

        Utils.toast(context, String.format("%s\nDownload%s", context.getString(R.string.image_download_complete), fileName));
    }
}
