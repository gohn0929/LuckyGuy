package com.gohn.luckyguy.util;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.gohn.luckyguy.common.ApplicationProvider;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

/**
 * Created by Vayne on 16. 8. 9..
 */
public class ImageUtil {

    String uri;

    DisplayImageOptions.Builder optionBuilder = new DisplayImageOptions.Builder();
    ImageLoadingListener listener;

    public ImageUtil(String uri) {
        this.uri = uri;
    }

    public static ImageUtil load(String uri) {
        if (ImageLoader.getInstance() == null)
            ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(ApplicationProvider.getContext()));

        ImageUtil imageUtil = new ImageUtil(uri);
        imageUtil.optionBuilder
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .displayer(new FadeInBitmapDisplayer(200))
//                .decodingOptions(resizeOptions)
                .preProcessor(new BitmapProcessor() {
                    @Override
                    public Bitmap process(Bitmap bmp) {

                        return Bitmap.createScaledBitmap(bmp, bmp.getWidth() * 2, bmp.getHeight() * 2, false);
                    }
                })
//                .postProcessor(new BitmapProcessor() {
//                    @Override
//                    public Bitmap process(Bitmap bmp) {
//                        int facter = 1;
//
//                        LLog.e("@@@@ " + bmp.getWidth() + " : " + bmp.getHeight());
//                        if (bmp.getWidth() < 400 || bmp.getHeight() < 400) {
//                            facter = 2;
//                        }
//                        return Bitmap.createScaledBitmap(bmp, bmp.getWidth() * 2, bmp.getHeight() * 2, false);
//                    }
//                })
                .considerExifParams(true);
        return imageUtil;
    }

    public void into(ImageView imageView) {
        if (uri == null) {
            LLog.e("ImageUtil uri is null");
        }

        if (!ImageLoader.getInstance().isInited())
            ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(ApplicationProvider.getContext()));

        ImageLoader.getInstance().displayImage(uri, imageView, optionBuilder.build(), listener);
    }

    public ImageUtil listen(ImageLoadingListener listener) {
        this.listener = listener;
        return this;
    }

    public ImageUtil useMemoryCache(boolean memoryCache) {
        optionBuilder.cacheInMemory(memoryCache);
        return this;
    }

    public ImageUtil useDiskCache(boolean diskCache) {
        optionBuilder.cacheOnDisk(diskCache);
        return this;
    }

    public ImageUtil considerExifParams(boolean consider) {
        optionBuilder.considerExifParams(consider);
        return this;
    }

    // with는 로드될때 띄우는 이미지
    public ImageUtil with(int resourceId) {
        optionBuilder.showImageOnLoading(resourceId);
        optionBuilder.showImageForEmptyUri(resourceId);
        optionBuilder.showImageOnFail(resourceId);
        return this;
    }

    public ImageUtil with(Drawable drawable) {
        optionBuilder.showImageOnLoading(drawable);
        optionBuilder.showImageForEmptyUri(drawable);
        optionBuilder.showImageOnFail(drawable);
        return this;
    }

    public ImageUtil fadeInDuration(int milliseconds) {
        optionBuilder.displayer(new FadeInBitmapDisplayer(milliseconds));
        return this;
    }
}
