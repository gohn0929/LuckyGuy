package com.gohn.luckyguy.util;

import android.content.Intent;

import com.gohn.luckyguy.activity.MainActivity;
import com.gohn.luckyguy.common.ApplicationProvider;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Vayne on 16. 8. 17..
 */
public class DB {
    public interface Update {
        void execute();
    }

    private static void restart() {
        Intent intent = new Intent(ApplicationProvider.getContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ApplicationProvider.getContext().startActivity(intent);
    }

    public static void update(Update update) {
        Realm realm = getRealmInstance();
        if (realm == null) {
            restart();
            return;
        }

        realm.beginTransaction();
        update.execute();
        realm.commitTransaction();
    }

    public static <T extends RealmObject> T storeData(T data) {
        Realm realm = getRealmInstance();
        if (realm == null) {
            restart();
            return null;
        }

        realm.beginTransaction();
        T realmObject = realm.copyToRealmOrUpdate(data);
        realm.commitTransaction();
        return realmObject;
    }

    public static <T extends RealmObject> List<T> storeDataList(List<T> data) {
        Realm realm = getRealmInstance();
        if (realm == null) {
            restart();
            return null;
        }

        realm.beginTransaction();
        List<T> realmObjectList = realm.copyToRealmOrUpdate(data);
        realm.commitTransaction();
        return realmObjectList;
    }

    public static <T extends RealmObject> RealmResults<T> getData(Class<T> type) {
        Realm realm = getRealmInstance();
        if (realm == null) {
            restart();
            return null;
        }
        RealmResults<T> results = realm.where(type).findAll();
        return results.sort("timestamp", Sort.DESCENDING);
    }

    public static <T extends RealmObject> RealmResults<T> getData(Class<T> type, String key, Integer compare) {
        Realm realm = getRealmInstance();
        if (realm == null) {
            restart();
            return null;
        }
        RealmResults<T> results = realm.where(type).equalTo(key, compare).findAll();
        return results.sort("timestamp", Sort.DESCENDING);
    }

    public static <T extends RealmObject> RealmResults<T> getData(Class<T> type, String key, String compare) {
        Realm realm = getRealmInstance();
        if (realm == null) {
            restart();
            return null;
        }
        RealmResults<T> results = realm.where(type).equalTo(key, compare).findAll();
        return results.sort("timestamp", Sort.DESCENDING);
    }

    public static <T extends RealmObject> RealmResults<T> getData(Class<T> type, String key, Boolean compare) {
        Realm realm = getRealmInstance();
        if (realm == null) {
            restart();
            return null;
        }
        RealmResults<T> results = realm.where(type).equalTo(key, compare).findAll();
        return results.sort("timestamp", Sort.DESCENDING);
    }

    public static <T extends RealmObject> T getFirstData(Class<T> type) {
        Realm realm = getRealmInstance();
        if (realm == null) {
            restart();
            return null;
        }

        T results = realm.where(type).findFirst();

        return results;
    }

    public static <T extends RealmObject> T getFirstData(Class<T> type, String key, String compare) {
        Realm realm = getRealmInstance();
        if (realm == null) {
            restart();
            return null;
        }

        T results = realm.where(type).equalTo(key, compare).findFirst();

        return results;
    }

    public static Realm getRealmInstance() {
        try {
            Realm realm = Realm.getDefaultInstance();
            return realm;
        } catch (Exception e) {
            return null;
        }
    }

    public static <T extends RealmObject> void deleteClass(Class<T> type) {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<T> results = realm.where(type).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteAllFromRealm();
            }
        });
    }
}
