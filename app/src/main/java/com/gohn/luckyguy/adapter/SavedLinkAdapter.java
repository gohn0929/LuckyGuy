package com.gohn.luckyguy.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.gohn.luckyguy.R;
import com.gohn.luckyguy.activity.BaseActivity;
import com.gohn.luckyguy.activity.ImageResultActivity;
import com.gohn.luckyguy.activity.MainActivity;
import com.gohn.luckyguy.activity.WebActivity;
import com.gohn.luckyguy.common.LDialog;
import com.gohn.luckyguy.data.QueryData;
import com.gohn.luckyguy.data.QueryItemData;
import com.gohn.luckyguy.enums.ListMode;
import com.gohn.luckyguy.enums.SearchMode;
import com.gohn.luckyguy.network.NetworkThread;
import com.gohn.luckyguy.network.Services;
import com.gohn.luckyguy.util.DB;
import com.gohn.luckyguy.util.ImageUtil;
import com.gohn.luckyguy.util.LLog;
import com.gohn.luckyguy.util.LTime;
import com.gohn.luckyguy.util.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by Vayne on 16. 9. 9..
 */
public class SavedLinkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private Context context;
    private List<QueryItemData> items;
    private ListMode listMode = ListMode.RECENT;
    private SearchMode currentSearchMode = SearchMode.WEB;
    private boolean onSearching = false;

    public SavedLinkAdapter(Context context) {
        this.context = context;
        this.items = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            return new SavedLinkViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_saved_link_item, parent, false));
        } else if (viewType == TYPE_HEADER) {
            return new HeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_saved_link_item_header, parent, false));
        } else {
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return items.size() + 1;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof HeaderViewHolder) {
            final HeaderViewHolder h = (HeaderViewHolder) holder;

            if (items.size() == 0) {
                h.txtNoResult.setVisibility(View.VISIBLE);
                h.txtNoResult.setText(listMode.getEmptyMessage(context));
            } else {
                h.txtNoResult.setVisibility(View.GONE);
            }

            h.btnClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    h.etQuery.setText("");
                    h.btnClear.setVisibility(View.INVISIBLE);
                }
            });

            h.etQuery.requestFocus();
            h.etQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        performSearch(h.etQuery, SearchMode.WEB);
                        return true;
                    }
                    return false;
                }
            });
            h.etQuery.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    h.btnClear.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            h.btnSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    performSearch(h.etQuery, SearchMode.WEB);
                }
            });
            h.btnSearchImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    performSearch(h.etQuery, SearchMode.IMAGE);
                }
            });

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.txt_recent:
                            listMode = ListMode.RECENT;
                            h.txtRecent.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                            h.txtFavorite.setTextColor(context.getResources().getColor(R.color.gray9));
                            setItems(DB.getData(QueryItemData.class, listMode.getKey(), true));
                            break;
                        case R.id.txt_favorite:
                            listMode = ListMode.FAVORITE;
                            h.txtRecent.setTextColor(context.getResources().getColor(R.color.gray9));
                            h.txtFavorite.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                            setItems(DB.getData(QueryItemData.class, listMode.getKey(), true));
                            break;
                        case R.id.txt_delete:
                            LDialog.showTwoButtonDialog(context, null, listMode.getDeleteMessage(context), new LDialog.TwoButtonClickListener() {
                                @Override
                                public void onClickOK() {
                                    RealmResults<QueryItemData> items = DB.getData(QueryItemData.class, listMode.getKey(), true);

                                    LLog.e("size : " + items.size());

                                    for (final QueryItemData item : items) {

                                        LLog.e(item.getTitle());
                                        DB.update(new DB.Update() {
                                            @Override
                                            public void execute() {
                                                if (listMode.equals(ListMode.RECENT)) {
                                                    item.setRecent(false);
                                                } else if (listMode.equals(ListMode.FAVORITE)) {
                                                    item.setFavorite(false);
                                                }

                                                // 둘다 포함되지 않는 링크는 디비에서 아예 삭제 한다.
                                                if (!item.isRecent() && !item.isFavorite()) {
                                                    item.deleteFromRealm();
                                                }
                                            }
                                        });
                                    }
                                    setItems(new ArrayList<QueryItemData>());
                                }

                                @Override
                                public void onClickCancel() {

                                }
                            });
                            break;
                    }
                }
            };

            h.txtRecent.setOnClickListener(clickListener);
            h.txtFavorite.setOnClickListener(clickListener);
            h.txtDelete.setOnClickListener(clickListener);

        } else if (holder instanceof SavedLinkViewHolder) {
            final QueryItemData data = items.get(position - 1);
            data.addChangeListener(changedListener);

            LLog.e(data.toString());

            final SavedLinkViewHolder h = (SavedLinkViewHolder) holder;

            h.card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, WebActivity.class);
                    intent.putExtra("title", data.getTitle());
                    intent.putExtra("link", data.getLink());
                    intent.putExtra("isSingle", true);
                    context.startActivity(intent);
                }
            });

            h.date.setText(new LTime(data.getTimestamp()).getFullDate());
            h.title.setText(data.getTitle());
            h.desc.setText(data.getSnippet());

            if (Utils.isNullOrEmpty(data.getSiteName())) {
                h.site.setVisibility(View.GONE);
            } else {
                h.site.setVisibility(View.VISIBLE);
                h.site.setText(data.getSiteName());
            }

            if (Utils.isNullOrEmpty(data.getThumbnail())) {
                h.thumbnail.setVisibility(View.GONE);
            } else {
                h.thumbnail.setVisibility(View.VISIBLE);
                ImageUtil.load(data.getThumbnail()).into(h.thumbnail);
            }

            h.favorite.setImageResource(data.isFavorite() ? R.drawable.ic_favorite_yellow : R.drawable.ic_favorite);
            data.addChangeListener(new RealmChangeListener<QueryItemData>() {
                @Override
                public void onChange(QueryItemData element) {
                    h.favorite.setImageResource(data.isFavorite() ? R.drawable.ic_favorite_yellow : R.drawable.ic_favorite);
                }
            });
            h.favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isFavorite = data.isFavorite();

                    if (listMode.equals(ListMode.RECENT)) {
                        if (isFavorite) {
                            Snackbar.make(v, R.string.main_favorite_removed, Snackbar.LENGTH_LONG)
                                    .setAction(R.string.action_cancel, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            DB.update(new DB.Update() {
                                                @Override
                                                public void execute() {
                                                    data.setFavorite(true);
                                                }
                                            });
                                        }
                                    })
                                    .show();

                            DB.update(new DB.Update() {
                                @Override
                                public void execute() {
                                    data.setFavorite(false);
                                }
                            });
                        } else {
                            Snackbar.make(v, R.string.main_favorite_added, Snackbar.LENGTH_LONG)
                                    .setAction(R.string.action_cancel, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            DB.update(new DB.Update() {
                                                @Override
                                                public void execute() {
                                                    data.setFavorite(false);
                                                }
                                            });
                                        }
                                    })
                                    .show();

                            DB.update(new DB.Update() {
                                @Override
                                public void execute() {
                                    data.setFavorite(true);
                                }
                            });
                        }
                    } else if (listMode.equals(ListMode.FAVORITE)) {
                        if (isFavorite) {
                            Snackbar.make(v, R.string.main_favorite_removed, Snackbar.LENGTH_LONG)
                                    .setAction(R.string.action_cancel, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            DB.update(new DB.Update() {
                                                @Override
                                                public void execute() {
                                                    data.setFavorite(true);
                                                }
                                            });
                                        }
                                    })
                                    .show();

                            DB.update(new DB.Update() {
                                @Override
                                public void execute() {
                                    data.setFavorite(false);
                                }
                            });
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;
        else
            return TYPE_ITEM;
    }

    public void setItems(List<QueryItemData> items) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    public ListMode getListMode() {
        return listMode;
    }

    private RealmChangeListener changedListener = new RealmChangeListener() {
        @Override
        public void onChange(Object element) {
            notifyDataSetChanged();
        }
    };

    public class SavedLinkViewHolder extends RecyclerView.ViewHolder {
        public View card;

        public TextView date;
        public TextView title;
        public TextView desc;
        public TextView site;
        public ImageView thumbnail;
        public ImageView favorite;

        public SavedLinkViewHolder(View view) {
            super(view);

            card = view.findViewById(R.id.card);

            date = (TextView) view.findViewById(R.id.txt_date);
            title = (TextView) view.findViewById(R.id.txt_title);
            desc = (TextView) view.findViewById(R.id.txt_desc);
            site = (TextView) view.findViewById(R.id.txt_site);
            thumbnail = (ImageView) view.findViewById(R.id.img_thumbnail);
            favorite = (ImageView) view.findViewById(R.id.img_favorite);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public View layout;
        public EditText etQuery;
        public View btnSearch;
        public View btnSearchImage;
        public View btnClear;
        public TextView txtRecent;
        public TextView txtFavorite;
        public TextView txtDelete;
        public TextView txtNoResult;

        public HeaderViewHolder(View view) {
            super(view);

            layout = view.findViewById(R.id.activity_main);
            etQuery = (EditText) view.findViewById(R.id.et_search);
            btnSearch = view.findViewById(R.id.btn_search);
            btnSearchImage = view.findViewById(R.id.btn_image);
            btnClear = view.findViewById(R.id.img_clear);

            txtRecent = (TextView) view.findViewById(R.id.txt_recent);
            txtFavorite = (TextView) view.findViewById(R.id.txt_favorite);
            txtDelete = (TextView) view.findViewById(R.id.txt_delete);
            txtNoResult = (TextView) view.findViewById(R.id.txt_no_result);
        }
    }

    private void performSearch(final EditText et, SearchMode searchMode) {
        if (onSearching) return;

        LLog.d(et.getText().toString());

        if (Utils.isNullOrEmpty(et.getText().toString())) {
            Utils.toast(context, context.getString(R.string.main_enter_query));
            return;
        }

        if (context instanceof BaseActivity) {
            ((BaseActivity) context).showProgress(true);
        }


        Utils.hideKeyboard(context, et);
        onSearching = true;
        currentSearchMode = searchMode;

        Services.search(context, et.getText().toString(), searchMode, new NetworkThread.OnTaskCompletedListener() {
            @Override
            public void onResult(String result) {
                LLog.d(result);
                QueryData queryData = new Gson().fromJson(result, QueryData.class);

                if (context instanceof BaseActivity) {
                    ((BaseActivity) context).showProgress(false);
                }

                String json = new Gson().toJson(queryData);

                LLog.d(json);

                if (queryData.getItems().size() > 0) {
                    if (currentSearchMode.equals(SearchMode.WEB)) {
                        Intent intent = new Intent(context, WebActivity.class);
                        intent.putExtra("query", et.getText().toString());
                        intent.putExtra("data", queryData);
                        ((Activity) context).startActivityForResult(intent, MainActivity.LINK_ADDED);
                    } else if (currentSearchMode.equals(SearchMode.IMAGE)) {
                        Intent intent = new Intent(context, ImageResultActivity.class);
                        intent.putExtra("query", et.getText().toString());
                        intent.putExtra("data", queryData);
                        ((Activity) context).startActivity(intent);
                    }
                } else {
                    Utils.toast(context, context.getString(R.string.main_no_result));
                }
                onSearching = false;
            }
        });
    }

    public void remove(View v, int position) {
        final QueryItemData data = items.get(position - 1);
        data.addChangeListener(changedListener);

        if (listMode.equals(ListMode.RECENT)) {
            Snackbar.make(v, R.string.main_recent_removed, Snackbar.LENGTH_LONG)
                    .setAction(R.string.action_cancel, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DB.update(new DB.Update() {
                                @Override
                                public void execute() {
                                    data.setRecent(true);
                                }
                            });
                        }
                    })
                    .show();

            DB.update(new DB.Update() {
                @Override
                public void execute() {
                    data.setRecent(false);
                }
            });
        } else {
            Snackbar.make(v, R.string.main_favorite_removed, Snackbar.LENGTH_LONG)
                    .setAction(R.string.action_cancel, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DB.update(new DB.Update() {
                                @Override
                                public void execute() {
                                    data.setFavorite(true);
                                }
                            });
                        }
                    })
                    .show();

            DB.update(new DB.Update() {
                @Override
                public void execute() {
                    data.setFavorite(false);
                }
            });
        }
    }
}