package com.gohn.luckyguy.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gohn.luckyguy.R;
import com.gohn.luckyguy.activity.FullImageActivity;
import com.gohn.luckyguy.common.LDialog;
import com.gohn.luckyguy.data.QueryItemData;
import com.gohn.luckyguy.util.LLog;
import com.gohn.luckyguy.util.Utils;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vayne on 16. 9. 9..
 */
public class ImageResultAdapter extends RecyclerView.Adapter<ImageResultAdapter.ImageResultViewHolder> {
    private Context context;
    private ArrayList<QueryItemData> items;
    private int screenWidth;

    public ImageResultAdapter(Context context) {
        this.context = context;
        this.items = new ArrayList<>();

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screenWidth = size.x;
    }

    @Override
    public ImageResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageResultViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_image_result_item, parent, false));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(final ImageResultViewHolder holder, final int position) {
        final QueryItemData item = items.get(position);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FullImageActivity.class);
                intent.putExtra("list", items);
                intent.putExtra("position", position);
                context.startActivity(intent);
            }
        });

        holder.layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                LDialog.showTwoButtonDialog(context, null, context.getString(R.string.image_download_dialog), new LDialog.TwoButtonClickListener() {
                    @Override
                    public void onClickOK() {
                        new TedPermission(context)
                                .setPermissionListener(new PermissionListener() {
                                    @Override
                                    public void onPermissionGranted() {
                                        Utils.downloadFromUrl(context, item.getLink());
                                    }

                                    @Override
                                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                        Utils.toast(context, context.getString(R.string.image_permission_deny));
                                    }


                                })
                                .setDeniedMessage(R.string.image_permission_request)
                                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                .check();
                    }

                    @Override
                    public void onClickCancel() {

                    }
                });
                return false;
            }
        });

        float ratio = (float) item.getImageHeight() / (float) item.getImageWidth();
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(screenWidth, (int) (screenWidth * ratio));
        if (position == items.size() - 1) params.bottomMargin = Utils.dp2px(context, 8);
        holder.imgResult.setLayoutParams(params);

        LLog.e(String.format("[width : %d][height : %d][context link : %s]", item.getImageWidth(), item.getImageHeight(), item.getContextLink()));

        Glide.with(context).load(item.getLink()).into(holder.imgResult);
    }

    public void setItems(ArrayList<QueryItemData> items) {
        for (QueryItemData item : items) {
            item.proc();
        }
        this.items = items;
        notifyDataSetChanged();
    }

    public void addItems(List<QueryItemData> items) {
        for (QueryItemData item : items) {
            item.proc();
        }
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public class ImageResultViewHolder extends RecyclerView.ViewHolder {
        public View layout;
        public ImageView imgResult;

        public ImageResultViewHolder(View view) {
            super(view);

            layout = view.findViewById(R.id.layout);
            imgResult = (ImageView) view.findViewById(R.id.img_result);
        }
    }
}