package com.gohn.luckyguy.network;

/**
 * Created by Gohn on 2016. 10. 9..
 */

public class UrlContainer {
    public final static String YOUTUBE = "www.youtube.com";
    public final static String PLAY_STORE = "https://play.google.com/store/apps/";
    public final static String INSTAGRAM = "https://www.instagram.com/";
}
