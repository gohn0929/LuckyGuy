package com.gohn.luckyguy.network;

import android.content.Context;

import com.gohn.luckyguy.enums.SearchMode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vayne on 2016. 10. 14..
 */

public class Services {

    public static void search(Context context, String query, SearchMode searchMode, NetworkThread.OnTaskCompletedListener listener) {
        search(context, query, 0, searchMode, listener);
    }

    public static void search(Context context, String query, int start, SearchMode searchMode, NetworkThread.OnTaskCompletedListener listener) {
        Map<String, Object> params = new HashMap<>();
        params.put("key", "AIzaSyCdl8AY5RmYZmSN0ImA5NWv6kUMyl-zLhY");
        params.put("cx", "010299860704273232510:dy2gasypkim");
        params.put("q", query);
        params.put("alt", "json");
        if (searchMode.equals(SearchMode.IMAGE)) {
            params.put("searchType", searchMode.getName());
            if (start > 0) params.put("start", start);
        }

        NetworkThread connectAuth = new NetworkThread(context, NetworkThread.METHOD.GET)
                .setParams(params)
                .setListener(listener);
        connectAuth.execute("https://www.googleapis.com/customsearch/v1");
    }
}
