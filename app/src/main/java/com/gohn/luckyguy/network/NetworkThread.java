package com.gohn.luckyguy.network;

import android.content.Context;
import android.os.AsyncTask;

import com.gohn.luckyguy.util.LLog;

import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Vayne on 16. 8. 3..
 */
public class NetworkThread extends AsyncTask<String, Void, String> {
    public static enum METHOD {
        GET, POST, PUT
    }

    public interface OnTaskCompletedListener {
        void onResult(String result);
    }

    private final static String CONNECT_FAIL = "fail";

    private String TAG = "NetworkThread";
    private MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private Context context;
    private Map<String, Object> params;
    private RequestBody body;
    OnTaskCompletedListener listener;
    METHOD method;

    public NetworkThread(Context context, METHOD method) {
        super();
        this.context = context;
        this.method = method;
    }

    public NetworkThread setListener(OnTaskCompletedListener listener) {
        this.listener = listener;
        return this;
    }

    public NetworkThread setBody(String jsonString) {
        this.body = RequestBody.create(JSON, jsonString);
        return this;
    }

    public NetworkThread setParams(Map<String, Object> params) {
        this.params = params;
        return this;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... urls) {
        StringBuilder url = new StringBuilder(urls[0]);

        if (params != null) {
            if (params.size() > 0) {
                url.append("?");
                for (String key : params.keySet()) {
                    url.append(key);
                    url.append("=");
                    url.append(params.get(key));
                    url.append("&");
                }
            }
        }


        LLog.d("REQUEST : " + url.toString());

        OkHttpClient client = new OkHttpClient();
        try {
            Request request;
            switch (method) {
                case GET:
                    request = new Request.Builder().url(url.toString()).build();
                    break;
                case POST:
                    request = new Request.Builder().url(url.toString()).post(body).build();
                    break;
                case PUT:
                    request = new Request.Builder().url(url.toString()).put(body).build();
                    break;
                default:
                    return null;
            }

            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (Exception e) {
            LLog.e("OkHttpClient Request Exception : " + e.toString());
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);

        if (result == null) {
            LLog.e("OkHttpClient Response result is null");
        } else {
            listener.onResult(result);
        }
    }
}
