package com.gohn.luckyguy.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vayne on 2016. 10. 11..
 */

public class MetaTagData implements Serializable {

    @SerializedName("og:site_name")
    String siteName;
    @SerializedName("application-name")
    String applicationName;

    public String getSiteName() {
        if (siteName == null) siteName = "";
        return siteName;
    }

    public String getApplicationName() {
        return applicationName;
    }

    @Override
    public String toString() {
        return "MetaTagData{" +
                "siteName='" + getSiteName() + '\'' +
                "applicationName='" + getApplicationName() + '\'' +
                '}';
    }
}
