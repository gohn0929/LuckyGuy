package com.gohn.luckyguy.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Vayne on 2016. 10. 11..
 */

public class PageMapData implements Serializable {
    ArrayList<ThumbnailData> cse_thumbnail;
    ArrayList<MetaTagData> metatags;

    public ArrayList<ThumbnailData> getCseThumbnail() {
        if (cse_thumbnail == null) cse_thumbnail = new ArrayList<>();
        return cse_thumbnail;
    }

    public ArrayList<MetaTagData> getMetaTags() {
        if (metatags == null) metatags = new ArrayList<>();
        return metatags;
    }

    @Override
    public String toString() {
        return "PageMapData{" +
                "cse_thumbnail=" + getCseThumbnail().toString() +
                ", metatags=" + getMetaTags().toString() +
                '}';
    }
}
