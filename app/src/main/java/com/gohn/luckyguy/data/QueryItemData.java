package com.gohn.luckyguy.data;

import com.gohn.luckyguy.util.Utils;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Gohn on 2016. 10. 9..
 */

public class QueryItemData extends RealmObject implements Serializable {
    String title;
    String snippet;
    @PrimaryKey
    String link;
    @Ignore
    String displayLink;
    @Ignore
    PageMapData pagemap;
    String thumbnail;
    String siteName;
    @Ignore
    ImageData image;
    String contextLink;
    int imageWidth;
    int imageHeight;
    long timestamp;
    boolean isRecent;
    boolean isFavorite;

    public QueryItemData proc() {
        if (getPageMap().getCseThumbnail().size() > 0) {
            if (getPageMap().getCseThumbnail().get(0).getUrl() != null) {
                thumbnail = getPageMap().getCseThumbnail().get(0).getUrl();
            }
        }
        if (getPageMap().getMetaTags().size() > 0) {
            MetaTagData metaTagData = getPageMap().getMetaTags().get(0);
            if (!Utils.isNullOrEmpty(metaTagData.getSiteName())) {
                siteName = metaTagData.getSiteName();
            } else if (!Utils.isNullOrEmpty(metaTagData.getApplicationName())) {
                siteName = metaTagData.getApplicationName();
            } else if (!Utils.isNullOrEmpty(displayLink)) {
                siteName = displayLink;
            }
        } else if (!Utils.isNullOrEmpty(displayLink)) {
            siteName = displayLink;
        }

        if (getImage() != null) {
            contextLink = getImage().getContextLink();
            imageWidth = getImage().getWidth();
            imageHeight = getImage().getHeight();
        }
        return this;
    }

    public String getTitle() {
        return title;
    }

    public String getSnippet() {
        return snippet;
    }

    public String getLink() {
        return link;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getSiteName() {
        return siteName;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isRecent() {
        return isRecent;
    }

    public void setRecent(boolean recent) {
        isRecent = recent;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getDisplayLink() {
        return displayLink;
    }

    public String getApplicationName() {
        return displayLink;
    }

    public PageMapData getPageMap() {
        if (pagemap == null) pagemap = new PageMapData();
        return pagemap;
    }

    public ImageData getImage() {
        if (image == null) image = new ImageData();
        return image;
    }

    public String getContextLink() {
        return contextLink;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    @Override
    public String toString() {
        return String.format("[Title : %s][Link : %s][Thumbnail : %s][Site : %s]", title, link, thumbnail, siteName);
    }
}
