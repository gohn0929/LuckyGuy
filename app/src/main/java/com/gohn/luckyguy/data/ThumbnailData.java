package com.gohn.luckyguy.data;

import java.io.Serializable;

/**
 * Created by Vayne on 2016. 10. 11..
 */

public class ThumbnailData implements Serializable {
    public String getUrl() {
        if (src == null) src = "";
        return src;
    }

    String src;

    @Override
    public String toString() {
        return "ThumbnailData{" +
                "url='" + getUrl() + '\'' +
                '}';
    }
}
