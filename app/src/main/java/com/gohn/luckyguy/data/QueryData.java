package com.gohn.luckyguy.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Gohn on 2016. 10. 9..
 */

public class QueryData implements Serializable {
    ArrayList<QueryItemData> items;

    public ArrayList<QueryItemData> getItems() {
        if (items == null) items = new ArrayList<>();
        return items;
    }

    @Override
    public String toString() {
        return "QueryData{" +
                "items=" + items.toString() +
                '}';
    }
}
