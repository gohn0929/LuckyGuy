package com.gohn.luckyguy.data;

import java.io.Serializable;

/**
 * Created by Vayne on 2016. 10. 14..
 */

public class ImageData implements Serializable {
    String contextLink;
    int width;
    int height;

    public String getContextLink() {
        return contextLink;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
