package com.gohn.luckyguy.enums;

import android.content.Context;

import com.gohn.luckyguy.R;

/**
 * Created by Vayne on 2016. 10. 12..
 */

public enum ListMode {
    RECENT, FAVORITE;

    public String getKey() {
        switch (this) {
            case RECENT:
                return "isRecent";
            case FAVORITE:
                return "isFavorite";
            default:
                return null;
        }
    }

    public String getEmptyMessage(Context context) {
        switch (this) {
            case RECENT:
                return context.getString(R.string.main_recent_empty);
            case FAVORITE:
                return context.getString(R.string.main_favorite_empty);
            default:
                return null;
        }
    }

    public String getDeleteMessage(Context context) {
        switch (this) {
            case RECENT:
                return context.getString(R.string.main_recent_delete);
            case FAVORITE:
                return context.getString(R.string.main_favorite_delete);
            default:
                return null;
        }
    }
}
