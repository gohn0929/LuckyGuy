package com.gohn.luckyguy.enums;

/**
 * Created by Vayne on 2016. 10. 12..
 */

public enum SearchMode {
    WEB, IMAGE;

    public String getName() {
        switch (this) {
            case WEB:
                return "web";
            case IMAGE:
                return "image";
            default:
                return null;
        }
    }
}
