package com.gohn.luckyguy.activity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gohn.luckyguy.R;
import com.gohn.luckyguy.common.LDialog;
import com.gohn.luckyguy.data.QueryItemData;
import com.gohn.luckyguy.util.HackyViewPager;
import com.gohn.luckyguy.util.ImageUtil;
import com.gohn.luckyguy.util.Utils;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

/**
 * 나는 했어 목록을 위한 view 클래스
 */
public class FullImageActivity extends AppCompatActivity {
    private String TAG = "FullImgActivity";
    private HackyViewPager viewPager;
    private LoadPagerAdapter adapter;
    private ArrayList<QueryItemData> list;
    private int position;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_full);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= 21)
            getWindow().setStatusBarColor(Color.TRANSPARENT);

        Intent i = getIntent();
        if (i != null) {
            list = (ArrayList<QueryItemData>) i.getSerializableExtra("list");
            position = i.getIntExtra("position", 0);

        }

        viewPager = (HackyViewPager) findViewById(R.id.load_pager);
        adapter = new LoadPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        title = (TextView) findViewById(R.id.txt_photo_full_title);

        ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                title.setText((position + 1) + "/" + list.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        viewPager.addOnPageChangeListener(pageChangeListener);
        viewPager.setCurrentItem(position);
        pageChangeListener.onPageSelected(position);


        findViewById(R.id.img_photo_full_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private class LoadPagerAdapter extends FragmentStatePagerAdapter {

        public LoadPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            QueryItemData item = list.get(position);
            return FullImageFragment.newInstance(position, item.getLink());
        }

        @Override
        public int getCount() {
            return list.size();
        }
    }

    public static class FullImageFragment extends Fragment {
        private String TAG = "FullImgFragment";
        private View rootView;
        private int position;
        private String path;
        private ImageView iv;

        public static FullImageFragment newInstance(int position, String path) {
            Bundle args = new Bundle();
            FullImageFragment fragment = new FullImageFragment();
            args.putInt("position", position);
            args.putString("path", path);
            fragment.setArguments(args);
            return fragment;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            position = getArguments().getInt("position");
            path = getArguments().getString("path");
            rootView = inflater.inflate(R.layout.fragment_full_image, container, false);
            iv = (ImageView) rootView.findViewById(R.id.img_photo_full);
            if (!Utils.isNullOrEmpty(path)) {
                ImageUtil.load(path).into(iv);

                iv.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        LDialog.showTwoButtonDialog(getActivity(), null, getString(R.string.image_download_dialog), new LDialog.TwoButtonClickListener() {
                            @Override
                            public void onClickOK() {
                                new TedPermission(getActivity())
                                        .setPermissionListener(new PermissionListener() {
                                            @Override
                                            public void onPermissionGranted() {
                                                Utils.downloadFromUrl(getActivity(), path);
                                            }

                                            @Override
                                            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                                Utils.toast(getActivity(), getString(R.string.image_permission_deny));
                                            }


                                        })
                                        .setDeniedMessage(R.string.image_permission_request)
                                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                        .check();
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });
                        return false;
                    }
                });
            }
            return rootView;
        }
    }
}
