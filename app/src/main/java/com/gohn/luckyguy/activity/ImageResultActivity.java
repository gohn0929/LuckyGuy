package com.gohn.luckyguy.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.gohn.luckyguy.R;
import com.gohn.luckyguy.adapter.ImageResultAdapter;
import com.gohn.luckyguy.common.LDialog;
import com.gohn.luckyguy.data.QueryData;
import com.gohn.luckyguy.enums.SearchMode;
import com.gohn.luckyguy.network.NetworkThread;
import com.gohn.luckyguy.network.Services;
import com.gohn.luckyguy.util.LLog;
import com.google.gson.Gson;

/**
 * Created by Vayne on 2016. 10. 14..
 */

public class ImageResultActivity extends BaseActivity {

    ActionBar actionBar;

    String query;
    QueryData data;
    RecyclerView recyclerView;
    ImageResultAdapter adapter;

    private int page = 0;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_result);

        query = getIntent().getStringExtra("query");
        data = (QueryData) getIntent().getSerializableExtra("data");

        actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(query);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;

                            page++;
                            request();
                        }
                    }
                }
            }
        });

        adapter = new ImageResultAdapter(this);
        recyclerView.setAdapter(adapter);

        adapter.setItems(data.getItems());
    }

    @Override
    public void onBackPressed() {
        showExitDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                showExitDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void request() {
        showProgress(true);

        Services.search(this, query, page * 10 + 1, SearchMode.IMAGE, new NetworkThread.OnTaskCompletedListener() {
            @Override
            public void onResult(String result) {
                LLog.d(result);
                QueryData queryData = new Gson().fromJson(result, QueryData.class);

                String json = new Gson().toJson(queryData);
                LLog.d(json);

                adapter.addItems(queryData.getItems());
                loading = true;
                showProgress(false);
            }
        });
    }

    private void showExitDialog() {
        LDialog.showTwoButtonDialog(this, null, getString(R.string.image_exit), new LDialog.TwoButtonClickListener() {
            @Override
            public void onClickOK() {
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onClickCancel() {

            }
        });
    }
}
