package com.gohn.luckyguy.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.gohn.luckyguy.R;
import com.gohn.luckyguy.common.ChromeClient;
import com.gohn.luckyguy.common.LDialog;
import com.gohn.luckyguy.data.QueryData;
import com.gohn.luckyguy.data.QueryItemData;
import com.gohn.luckyguy.network.UrlContainer;
import com.gohn.luckyguy.util.DB;
import com.gohn.luckyguy.util.Utils;
import com.gohn.luckyguy.util.linkpreview.LinkPreviewCallback;
import com.gohn.luckyguy.util.linkpreview.SourceContent;
import com.gohn.luckyguy.util.linkpreview.TextCrawler;

/**
 * Created by Gohn on 2016. 10. 9..
 */

public class WebActivity extends BaseActivity {

    ActionBar actionBar;

    QueryData data;
    WebView webView;

    MenuItem menuPrev;
    MenuItem menuNext;

    String queryText;
    int currentPosition = 0;

    boolean isSingle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        isSingle = getIntent().getBooleanExtra("isSingle", false);

        actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.setWebViewClient(webViewClient);
        webView.setWebChromeClient(new ChromeClient());

        if (isSingle) {
            actionBar.setTitle(getIntent().getStringExtra("title"));
            webView.loadUrl(getIntent().getStringExtra("link"));
        } else {
            data = (QueryData) getIntent().getSerializableExtra("data");
            if (data == null) finish();

            queryText = getIntent().getStringExtra("query");
            actionBar.setTitle(getActionBarTitle());

            show(currentPosition);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_web, menu);
        menuPrev = menu.findItem(R.id.action_prev);
        menuPrev.setVisible(false);
        menuNext = menu.findItem(R.id.action_next);
        if (isSingle) menuNext.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (webView != null && webView.canGoBack()) {
            webView.goBack();
        } else {
            showExitDialog();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                showExitDialog();
                break;
            case R.id.action_prev:
                if (currentPosition == 0) {
                    Utils.toast(this, getString(R.string.web_no_result_prev));
                } else {
                    show(currentPosition - 1);
                }
                break;
            case R.id.action_next:
                if (currentPosition == data.getItems().size() - 1) {
                    Utils.toast(this, getString(R.string.web_no_result_next));
                } else {
                    show(currentPosition + 1);
                }
                break;
            case R.id.action_favorite:
                final String currentUrl = webView.getUrl();
                final QueryItemData qData = DB.getFirstData(QueryItemData.class, "link", currentUrl);
                if (qData == null) {

                    TextCrawler textCrawler = new TextCrawler();
                    textCrawler.makePreview(new LinkPreviewCallback() {
                        @Override
                        public void onPre() {

                        }

                        @Override
                        public void onPos(SourceContent sourceContent, boolean isNull) {
                            QueryItemData newData = new QueryItemData();
                            newData.setTimestamp(System.currentTimeMillis());
                            newData.setFavorite(true);

                            if (sourceContent.getImages().size() > 0) {
                                newData.setThumbnail(sourceContent.getImages().get(0));
                            }

                            if (!Utils.isNullOrEmpty(sourceContent.getTitle())) {
                                newData.setTitle(sourceContent.getTitle());
                            }

                            if (!Utils.isNullOrEmpty(sourceContent.getDescription())) {
                                newData.setSnippet(sourceContent.getDescription());
                            }

                            String link = currentUrl;
                            if (!link.contains("http://") && !link.contains("https://")) {
                                link = "http://" + link;
                            }
                            newData.setLink(link);

                            DB.storeData(newData);
                            setResult(RESULT_OK);
                            Utils.toast(WebActivity.this, getString(R.string.web_favorite_added));
                        }
                    }, currentUrl);
                } else {
                    DB.update(new DB.Update() {
                        @Override
                        public void execute() {
                            qData.setFavorite(true);
                            setResult(RESULT_OK);
                            Utils.toast(WebActivity.this, getString(R.string.web_favorite_added));
                        }
                    });
                }
                break;
            case R.id.action_share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, data.getItems().get(currentPosition).getLink());
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.web_share_url)));
                break;
            case R.id.action_other:
                otherBrower(webView.getUrl());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void show(int position) {
        QueryItemData queryItemData = data.getItems().get(position);
        queryItemData.setTimestamp(System.currentTimeMillis());
        queryItemData.setRecent(true);

        DB.storeData(queryItemData.proc());

        currentPosition = position;
        actionBar.setTitle(getActionBarTitle());

        if (menuPrev != null) menuPrev.setVisible(position != 0);
        if (menuNext != null) menuNext.setVisible(data.getItems().size() > position + 1);

        String link = new String(queryItemData.getLink());

        webView.clearView();
        webView.loadUrl(link);
    }

    private void showExitDialog() {
        LDialog.showTwoButtonDialog(this, null, getString(R.string.web_exit), new LDialog.TwoButtonClickListener() {
            @Override
            public void onClickOK() {
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onClickCancel() {

            }
        });
    }

    private void otherBrower(String url) {
        String link = new String(url);
        if (link.contains("www.youtube.com")) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            startActivity(intent);
        } else if (link.contains(UrlContainer.PLAY_STORE)) {
            link.replace(UrlContainer.PLAY_STORE, "market://");
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            startActivity(intent);
        } else if (link.contains(UrlContainer.INSTAGRAM)) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            intent.setPackage("com.instagram.android");
            startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(link));
            startActivity(intent);
        }
    }

    private WebViewClient webViewClient = new WebViewClient() {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }
    };

    private String getActionBarTitle() {
        return String.format("%s (%d/%d)", queryText, currentPosition + 1, data.getItems().size());
    }
}