package com.gohn.luckyguy.activity;

import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.gohn.luckyguy.R;
import com.gohn.luckyguy.adapter.SavedLinkAdapter;
import com.gohn.luckyguy.common.BackPressCloseHandler;
import com.gohn.luckyguy.data.QueryItemData;
import com.gohn.luckyguy.util.DB;
import com.gohn.luckyguy.util.LLog;

import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

import static com.gohn.luckyguy.common.CommonData.ISLAUNCHED;

public class MainActivity extends BaseActivity {
    public final static int LINK_ADDED = 999;
    BackPressCloseHandler backPressCloseHandler = new BackPressCloseHandler(this);
    RecyclerView recyclerView;
    SavedLinkAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        backPressCloseHandler = new BackPressCloseHandler(this);

        if (!ISLAUNCHED) {
            RealmConfiguration config = new RealmConfiguration
                    .Builder(this)
                    .migration(migration)
                    .schemaVersion(2)
                    .build();
            Realm.setDefaultConfiguration(config);
            ISLAUNCHED = true;
        }
        initView();
    }

    RealmMigration migration = new RealmMigration() {
        @Override
        public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
            RealmSchema schema = realm.getSchema();

            if (oldVersion == 0) {
                LLog.e("oldVersion == 0");
                oldVersion++;
            }

            if (oldVersion == 1) {
                LLog.e("oldVersion == 1");
                RealmObjectSchema queryItemData = schema.get("QueryItemData");
                queryItemData.addField("contextLink", String.class);
                queryItemData.addField("imageWidth", int.class);
                queryItemData.addField("imageHeight", int.class);
            }
        }
    };

    @Override
    public void onBackPressed() {
        backPressCloseHandler.onBackPressed();
    }

    private void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new SavedLinkAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.setItems(DB.getData(QueryItemData.class, adapter.getListMode().getKey(), true));


        setSwipe();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LINK_ADDED) {
            if (resultCode == RESULT_OK) {
                adapter.setItems(DB.getData(QueryItemData.class, adapter.getListMode().getKey(), true));
            }
        }
    }

    private void setSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                        int position = viewHolder.getAdapterPosition();
                        if (position == 0) return ItemTouchHelper.ACTION_STATE_IDLE;

                        return super.getSwipeDirs(recyclerView, viewHolder);
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        int swipedPosition = viewHolder.getAdapterPosition();
                        adapter.remove(recyclerView, swipedPosition);
                    }

                    @Override
                    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                    }
                };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);
    }
}
