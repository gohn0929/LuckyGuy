package com.gohn.luckyguy.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.gohn.luckyguy.R;

/**
 * Created by Gohn on 2016. 10. 10..
 */

public class BaseActivity extends AppCompatActivity {

    View progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(R.layout.activity_base);

        ViewGroup root = (ViewGroup) findViewById(R.id.root);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutCategory = inflater.inflate(layoutResID, null);
        root.addView(layoutCategory, 0);

        progress = findViewById(R.id.progress);
    }

    public void showProgress(boolean isShow) {
        progress.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }
}